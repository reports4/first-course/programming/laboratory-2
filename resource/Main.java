import pokemons.*;
import ru.ifmo.se.pokemon.Battle;

public class Main {

	public static void main(String[] args) {
		Battle battle = new Battle();
		battle.addAlly(new HoOh("PokA1"));
//		battle.addAlly(new Poliwhirl("PokA2"));
		battle.addAlly(new MeowsticMale("PokA3"));
		battle.addFoe(new Espurr("PokF1"));
		battle.addFoe(new Poliwag("PokF2"));
//		battle.addFoe(new Politoed("PokF3"));
		battle.go();
	}
}
