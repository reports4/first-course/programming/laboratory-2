package attack;

import ru.ifmo.se.pokemon.PhysicalMove;
import ru.ifmo.se.pokemon.Pokemon;
import ru.ifmo.se.pokemon.Status;
import ru.ifmo.se.pokemon.Type;

public class Facade extends PhysicalMove {

	public Facade() {super(Type.NORMAL, 70, 1);}
	
	@Override
	protected void applyOppDamage(Pokemon def, double damage) {
		Status s = def.getCondition();
		if(s == Status.BURN || s == Status.PARALYZE || s == Status.POISON)
			damage = 2*damage;
		super.applyOppDamage(def, damage);
	}
	
	@Override
	protected String describe() {return "кастует Facade";}
}