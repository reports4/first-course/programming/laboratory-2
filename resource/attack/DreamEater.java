package attack;

import ru.ifmo.se.pokemon.Pokemon;
import ru.ifmo.se.pokemon.SpecialMove;
import ru.ifmo.se.pokemon.Status;
import ru.ifmo.se.pokemon.Type;

public class DreamEater extends SpecialMove {

	public DreamEater() {super(Type.PSYCHIC, 100, 1);}
	
	@Override
	protected void applyOppDamage(Pokemon def, double damage) {
		if(def.getCondition() == Status.SLEEP)
			super.applyOppDamage(def, damage);
	}
	
	@Override
	protected String describe() {return "кастует Dream Eater";}
}
