package attack;

import ru.ifmo.se.pokemon.Effect;
import ru.ifmo.se.pokemon.Pokemon;
import ru.ifmo.se.pokemon.SpecialMove;
import ru.ifmo.se.pokemon.Stat;
import ru.ifmo.se.pokemon.Type;

public class ShadowBall extends SpecialMove{

	public ShadowBall() {super(Type.GHOST, 80, 1);}
	
	@Override
	protected void applyOppEffects(Pokemon p) {
		p.addEffect(new Effect().chance(0.2).stat(Stat.SPECIAL_DEFENSE, -1));
	}
	
	@Override
	protected String describe() {return "кастует Shadow Ball";}
}
