package attack;

import ru.ifmo.se.pokemon.Effect;
import ru.ifmo.se.pokemon.Pokemon;
import ru.ifmo.se.pokemon.SpecialMove;
import ru.ifmo.se.pokemon.Stat;
import ru.ifmo.se.pokemon.Type;

public class Psychic extends SpecialMove{

	public Psychic() {super(Type.PSYCHIC, 90, 1);}
	
	@Override
	protected void applyOppEffects(Pokemon p) {
		p.addEffect(new Effect().chance(0.1).stat(Stat.SPECIAL_DEFENSE, -1));
	}
	
	@Override
	protected String describe() {return "кастует Psychic";}
}