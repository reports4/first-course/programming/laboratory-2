package attack;

import ru.ifmo.se.pokemon.PhysicalMove;
import ru.ifmo.se.pokemon.Pokemon;
import ru.ifmo.se.pokemon.Type;

public class DoubleSlap extends PhysicalMove{

	public DoubleSlap() {super(Type.NORMAL, 15, 0.85);}
	
	@Override
	protected void applyOppDamage(Pokemon def, double damage) {
		damage = Math.random()*4+1;
		super.applyOppDamage(def, damage);
	}
	
	@Override
	protected String describe() {return "кастует Double Slap";}
}