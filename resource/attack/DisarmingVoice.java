package attack;

import ru.ifmo.se.pokemon.SpecialMove;
import ru.ifmo.se.pokemon.Type;

public class DisarmingVoice extends SpecialMove{

	public DisarmingVoice() {super(Type.FAIRY, 0, 1);}
	
	@Override
	protected String describe() {return "кастует Disarming Voice";}
}
