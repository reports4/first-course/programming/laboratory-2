package attack;

import ru.ifmo.se.pokemon.Effect;
import ru.ifmo.se.pokemon.PhysicalMove;
import ru.ifmo.se.pokemon.Pokemon;
import ru.ifmo.se.pokemon.Status;
import ru.ifmo.se.pokemon.Type;

public class BodySlam extends PhysicalMove{
	
	public BodySlam() { super(Type.NORMAL, 85, 1); }
	
	@Override
	protected void applyOppEffects(Pokemon p) {
		p.addEffect(new Effect().chance(0.3D).condition(Status.PARALYZE));
	}
	
	@Override
	protected String describe() {return "кастует Body Slam";}
}
