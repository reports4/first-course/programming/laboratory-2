package pokemons;

import attack.*;
import ru.ifmo.se.pokemon.Pokemon;
import ru.ifmo.se.pokemon.Type;

public class Espurr extends Pokemon {
	
	private double hp = 62;
	private double att = 48; 
	private double def = 54; 
	private double spAtt = 63; 
	private double spDef = 60; 
	private double speed = 68;
	
	public Espurr(String name, int level) {
		super(name, level);
		this.setStats(hp, att, def, spAtt, spDef, speed);
		this.setMove(new Psychic(), new DreamEater(), new DisarmingVoice());
		this.setType(Type.PSYCHIC);
	}
	
	public Espurr(String name) {this(name, 2);}
	public Espurr() {this("Espurr");}
}
