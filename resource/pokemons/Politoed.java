package pokemons;

import attack.Facade;

public class Politoed extends Poliwhirl {
	
	private double hp = 62;
	private double att = 48; 
	private double def = 54; 
	private double spAtt = 63; 
	private double spDef = 60; 
	private double speed = 68;
	
	public Politoed(String name, int level) {
		super(name, level);
		this.setStats(hp, att, def, spAtt, spDef, speed);
		this.addMove(new Facade());
	}
	
	public Politoed(String name) {this(name, 2);}
	public Politoed() {this("Politoed");}
}
