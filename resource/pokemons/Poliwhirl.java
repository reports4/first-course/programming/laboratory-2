package pokemons;

import attack.BodySlam;

public class Poliwhirl extends Poliwag{

	private double hp = 65;
	private double att = 65; 
	private double def = 65; 
	private double spAtt = 50; 
	private double spDef = 50; 
	private double speed = 90;
	
	public Poliwhirl(String name, int level) {
		super(name, level);
		this.setStats(hp, att, def, spAtt, spDef, speed);
		this.addMove(new BodySlam());
	}
	
	public Poliwhirl(String name) {this(name, 25);}
	public Poliwhirl() {this("Poliwhirl");}
}
