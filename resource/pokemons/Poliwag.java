package pokemons;

import attack.DoubleSlap;
import attack.DoubleTeam;
import ru.ifmo.se.pokemon.Pokemon;
import ru.ifmo.se.pokemon.Type;

public class Poliwag extends Pokemon{

	private double hp = 40;
	private double att = 50; 
	private double def = 40; 
	private double spAtt = 40; 
	private double spDef = 40; 
	private double speed = 90;
	
	public Poliwag(String name, int level) {
		super(name, level);
		this.setStats(hp, att, def, spAtt, spDef, speed);
		this.setMove(new DoubleSlap(), new DoubleTeam());
		this.setType(Type.WATER);
	}
	
	public Poliwag(String name) {this(name, 2);}
	public Poliwag() {this("Poliwag");}
}
