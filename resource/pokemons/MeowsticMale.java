package pokemons;

import attack.ShadowBall;

public class MeowsticMale extends Espurr{

	private double hp = 74;
	private double att = 48; 
	private double def = 76; 
	private double spAtt = 83; 
	private double spDef = 81; 
	private double speed = 104;
	
	public MeowsticMale(String name, int level) {
		super(name, level);
		this.setStats(hp, att, def, spAtt, spDef, speed);
		this.addMove(new ShadowBall());
	}
	
	public MeowsticMale(String name) {this(name, 25);}
	public MeowsticMale() {this("MeowsticMale");}
}
