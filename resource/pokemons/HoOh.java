package pokemons;

import attack.*;
import ru.ifmo.se.pokemon.Pokemon;
import ru.ifmo.se.pokemon.Type;

public class HoOh extends Pokemon{
	
	private double hp = 106;
	private double att = 130; 
	private double def = 90; 
	private double spAtt = 110; 
	private double spDef = 154; 
	private double speed = 90;
	
	public HoOh(String name, int level) {
		super(name, level);
		this.setStats(hp, att, def, spAtt, spDef, speed);
		this.setMove(new Swagger(), new DoubleTeam(), new Bulldoze(), new Overheat());
		this.setType(Type.FIRE, Type.FLYING);
	}
	
	public HoOh(String name) {	this(name, 2);}
	public HoOh() {this("Ho-Oh");}
}
